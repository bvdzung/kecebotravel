/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : kecebotravel

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-02-12 23:51:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for about
-- ----------------------------
DROP TABLE IF EXISTS `about`;
CREATE TABLE `about` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `body_en` text COLLATE utf8_unicode_ci NOT NULL,
  `body_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tile_fr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of about
-- ----------------------------
INSERT INTO `about` VALUES ('1', '<p style=\"text-align: justify;\">Pu Luong Nature Reserve, 200 km from Hanoi, has an evident touristic value thanks to its primary forests, its waterfalls, its terraced rice fields and Thais villages and this is a great destination for a few days trips from Hanoi.</p>\r\n<p style=\"text-align: justify;\">Ban Hi&ecirc;u Garden Logde is situated in Hieu village &ndash; in the core of Pu Luong Nature Reserve &ndash; where you can find yourself in the middle of a natural wild forest, emerging in the cool water of Hieu waterfalls et admiring the breathtaking terraced rice fields while staying with Thai.</p>\r\n<p style=\"text-align: justify;\">The lodge i​s&nbsp;located in the moutainside, lovely wedged between two waterfalls &ndash; a hidden germ to explore. The Thai traditions, the friendly atmosphere and the professionalism of local people guarantee a pleasan​t tonkin stay.</p>\r\n<p style=\"text-align: justify;\">The best seasons to visit Ban Hieu is from September to November or February until May when the yellow rice fields &nbsp;efore harvest show an amazing view before you and the weather is pleasant.</p>\r\n<p style=\"text-align: justify;\"><em><strong>What to explore around Ban Hieu Garden Lodge?</strong></em></p>\r\n<ul style=\"list-style-type: square; text-align: justify;\">\r\n<li>The Hieu waterfall is just 50m from our lodge where you can find a stunning natural pool &ndash; suitable for swimming. You can also walk to the upstream to discover even more waterfalls.</li>\r\n<li>There are lots of place to discover by trekking or on motorcycle:\r\n<ul>\r\n<li style=\"text-align: justify;\">Visit Pho Doan market with lots of local products (only in the morning).</li>\r\n<li style=\"text-align: justify;\">Hieu village (original) on top of the mountain with a panorama view to the valley.</li>\r\n<li style=\"text-align: justify;\">Visit Khuyn village &ndash; 5km from Hieu village or Kho Muong &ndash; 5h trekking.</li>\r\n<li style=\"text-align: justify;\">Visit Son &ndash; Ba &ndash; Muoi, 3 villages in the neigborhood. On the way, you will surely be amazed by the breathtaking view to the mountains and valleys of Pu Luong.</li>\r\n</ul>\r\n</li>\r\n</ul>\r\n<p style=\"text-align: justify;\"><em><strong>How to get there?</strong></em></p>\r\n<p style=\"text-align: justify;\">Ban Hieu Garden Lodge is located at Ban Hieu, Ba Thuoc, Thanh Hoa Province. From Hanoi, please follow the Highway Lang - Hoa Lac (or through Ha Dong) and keep up on Ho Chi Minh Highway to Cam Thuy (Thanh Hoa) - about 4 hours driving. hen from Cam Thuy to Canh Nang town - Pho Doan.</p>\r\n<p style=\"text-align: justify;\">From here, you must leave your car and walk about 5 km on a trail to Ban Hieu. The roadside scene is amazing with terrace rice fields, streams, small waterfalls, etc. If walking is not your thing, then you can order motortaxi in Pho Doan for 15min to Ban Hieu. Or you travel on a motobike, it would be a very interesting route to explore &lsquo;till the gate of our lodge.</p><iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d955006.2565778602!2d104.8079402!3d20.7713262!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313402e8ae1a5b51%3A0xd53f19523eb02575!2zQuG6o24gSGnDqnU!5e0!3m2!1sen!2s!4v1485197761460\"\r\n                        width=\"100%\" height=\"500px\"\r\n                        frameborder=\"0\" style=\"border:0\" allowfullscreen>\r\n\r\n                </iframe>', '<p style=\"text-align: justify;\">Le charmant et confortable lodge Ban Hi&ecirc;u Garden au village de Hi&ecirc;u se situe dans la r&eacute;serve naturelle de Pu Luong dans la province de Thanh Hoa, &agrave; environ 200 km de Hanoi ayant une valeur touristique indiscutable avec ses for&ecirc;ts primaires, ses cascades, ses rizi&egrave;res en terrasse et des villages Thais. Les traditions Thai, la convivialit&eacute; et le professionnalisme des locaux vous assurent un s&eacute;jour de qualit&eacute;.</p>\r\n<p style=\"text-align: justify;\">Ban Hieu Garden Lodge est situ&eacute; &agrave; flanc de montage, joliment enclav&eacute; entre deux chutes d&rsquo;eau avec une maison sur pilotis et 2 bungalows.</p>\r\n<p style=\"text-align: justify;\">La meilleure saison pour visiter Ban Hieu est du mois de septembre au mois de novembre ou du mois de f&eacute;vrier jusqu&rsquo;au mois de mai o&ugrave; les champs de riz jaunes avant la r&eacute;colte montrent une vue &eacute;tonnante et le temps est tr&egrave;s agr&eacute;able.</p>\r\n<p style=\"text-align: justify;\"><em><strong>&Agrave; d&eacute;couvrir:</strong></em></p>\r\n<ul style=\"list-style-type: circle; text-align: justify;\">\r\n<li>Les cascades de Hieu sont juste 50m de notre lodge o&ugrave; vous trouverez une piscine naturelle magnifique. Vous pourriez marcher &agrave; l&rsquo;amont pour d&eacute;couvrir plus de chutes d&rsquo;eau.</li>\r\n<li>Plusieurs de lieux &agrave; d&eacute;couvrir en trekking ou en moto:\r\n<ul>\r\n<li>Le march&eacute; de Pho Doan (10km de Ban Hieu Garden Lodge) avec ses produits locaux (matin&eacute;e)</li>\r\n<li>Le village de Hieu (original) au sommet de la montage donnant une vue panoramique sur la vall&eacute;e</li>\r\n<li>Le village de Khuyn &ndash; 5km de Ban Hieu ou Kho Muong &ndash; 5h de trekking</li>\r\n<li>Les villages Son-Ba- Muoi dans le voisinage. Sur le route, vous serez certainement &eacute;tonn&eacute;s par la vue impeccable sur les montages et la vall&eacute;e de Pu Luong.</li>\r\n</ul>\r\n</li>\r\n</ul>\r\n<p style=\"text-align: justify;\"><em><strong>Comment y aller?</strong></em></p>\r\n<p style=\"text-align: justify;\">Ban Hieu Garden Lodge se situe au village de Hieu, Ba Thuoc, Thanh Hoa. De Hanoi, suivez l&rsquo;autoroute Lang &ndash; Hoa Lac (ou par Ha Dong) et continuez sur la route Hochiminh jusqu&rsquo;&agrave; Cam Thuy (Thanh Hoa) &ndash; environ 4h de route et puis de Cam Thuy &agrave; la ville Canh Nang &ndash; Pho Doan. A partir de ce point, vous devriez laisser votre voiture et faire un trek l&eacute;ger (5km) pour arriver au village de Hieu. La sc&egrave;ne sur la route est tr&egrave;s jolie montrant des rizi&egrave;res en terrace, des ruisseaux, ascades, etc. Vous pourriez &eacute;ventuellment demander un motor taxi (g&eacute;r&eacute; par les locaux).</p><iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d955006.2565778602!2d104.8079402!3d20.7713262!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313402e8ae1a5b51%3A0xd53f19523eb02575!2zQuG6o24gSGnDqnU!5e0!3m2!1sen!2s!4v1485197761460\"\r\n                        width=\"100%\" height=\"500px\"\r\n                        frameborder=\"0\" style=\"border:0\" allowfullscreen>\r\n\r\n                </iframe>', null, null, null, 'À décrovrir:');

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `body_en` text COLLATE utf8_unicode_ci NOT NULL,
  `body_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of contacts
-- ----------------------------
INSERT INTO `contacts` VALUES ('1', '<p><strong>BAN HIEU GARDEN LODGE</strong></p>\r\n<p>Address : Hieu village, Cam Thuy Commune, Ba Thuoc District, Thanh Hoa City, Vietnam</p>\r\n<p>Tel : <a href=\"tel:+84904298536\">+84 904 298 536</a>&nbsp;/ <a href=\"tel:+84983362787\">+84 983 362 787</a>&nbsp;(English, French, Vietnamese)</p>\r\n<p>Email :<a href=\"mailto:banhieugarden@gmail.com\">banhieugarden@gmail.com</a></p>\r\n<p>Facebook : <a title=\"https://www.facebook.com/Ban-Hieu- Garden-Lodge-  535151283327796/?ref=aymt_homepage_panel\" href=\"https://www.facebook.com/Ban-Hieu-%20Garden-Lodge-  535151283327796/?ref=aymt_homepage_panel\">https://www.facebook.com/Ban-Hieu- Garden-Lodge-</a></p>\r\n<p><a title=\"https://www.facebook.com/Ban-Hieu- Garden-Lodge-  535151283327796/?ref=aymt_homepage_panel\" href=\"https://www.facebook.com/Ban-Hieu-%20Garden-Lodge-  535151283327796/?ref=aymt_homepage_panel\">535151283327796/?ref=aymt_homepage_panel</a></p>\r\n<p>Flickr :&nbsp;<a href=\"https://www.flickr.com/photos/134819256@N02/\">https://www.flickr.com/photos/134819256@N02/</a></p>', '<p><strong>BAN HIEU GARDEN LODGE</strong></p>\r\n<p>Address : Hieu village, Cam Thuy Commune, Ba Thuoc District, Thanh Hoa City, Vietnam</p>\r\n<p>T&eacute;l&nbsp;: <a href=\"tel:+84904298536\">+84 904 298 536</a>&nbsp;/ <a href=\"tel:+84983362787\">+84 983 362 787</a>&nbsp;(English, French, Vietnamese)</p>\r\n<p>Email :<a href=\"mailto:banhieugarden@gmail.com\">banhieugarden@gmail.com</a></p>\r\n<p>Facebook : <a title=\"https://www.facebook.com/Ban-Hieu- Garden-Lodge-  535151283327796/?ref=aymt_homepage_panel\" href=\"https://www.facebook.com/Ban-Hieu-%20Garden-Lodge-  535151283327796/?ref=aymt_homepage_panel\">https://www.facebook.com/Ban-Hieu- Garden-Lodge-</a></p>\r\n<p><a title=\"https://www.facebook.com/Ban-Hieu- Garden-Lodge-  535151283327796/?ref=aymt_homepage_panel\" href=\"https://www.facebook.com/Ban-Hieu-%20Garden-Lodge-  535151283327796/?ref=aymt_homepage_panel\">535151283327796/?ref=aymt_homepage_panel</a></p>\r\n<p>Flickr :&nbsp;<a href=\"https://www.flickr.com/photos/134819256@N02/\">https://www.flickr.com/photos/134819256@N02/</a></p>', '2017-01-10 05:42:10', '2017-01-10 05:42:13');

-- ----------------------------
-- Table structure for detail
-- ----------------------------
DROP TABLE IF EXISTS `detail`;
CREATE TABLE `detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `body_en` text COLLATE utf8_unicode_ci NOT NULL,
  `body_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of detail
-- ----------------------------
INSERT INTO `detail` VALUES ('1', '<p>The stilt house was built with tradition Thai&rsquo;s structure and perfectly designed for a group from 4 up to 8-10 pax with 2 private Double rooms, 1 Double bed and 2 Single mattresses (1.2m x 2m) in open space. All mattresses used are Dunlopillo with qualified draps and pillows toensure your best sleep.</p>\r\n<p><strong>Amenities:</strong> <em>Chais, table, fresh flowers, Games (Dart, Chess, Poker, etc. and others games for children), Bookshelf, Books in Vietnamese and other languages, trash can, hangers, fan, ceiling fan, lamps, leaf fans, conical hats, etc.</em></p><p>In order to remain the traditional design of Thai people, the sanitary block was built outside and in a common space with 2 bathrooms and 2 toilets artfully built of stone, wood and latanier leaves.</p>\r\n', '<p>La maison sur pilotis a &eacute;t&eacute; construite avec la structure tha&iuml;landaise traditionnelle et parfaitement con&ccedil;ue pour un groupe de 4 &agrave; 8-10 pax avec 2 chambres doubles priv&eacute;es, 1 lit double et 2 matelas simples (1.2m x 2m) en plein air. Tous les matelas utilis&eacute;s sont Dunlopillo avec draps et oreillers qualifi&eacute;s pour assurer votre meilleur sommeil.</p>\r\n<p><strong>Facilit&eacute;s:</strong> <em>Chais, table, fleurs fra&icirc;ches, Jeux (Dart, Chess, Poker, etc. et autres jeux pour enfants), &eacute;tag&egrave;re, Livres en vietnamien et autres langues, poubelles, Chapeaux coniques, etc.</em></p><p>Afin de maintenir la conception traditionnelle des Tha&iuml;s, le bloc sanitaire est construit &agrave; l&rsquo;ext&eacute;rieur et dans un espace commun avec 2 salles de bain et 2 toilettes esth&eacute;tiquement construites en pierre, bois et feuilles de latanier.</p>', '2017-01-10 06:57:39', '2017-01-10 06:57:42', 'Stilt house', 'Maison sur pilotis', '/images/image_bt1.png', '/images/slide.png');
INSERT INTO `detail` VALUES ('2', '<p>Each bungalow (30m2 including the balcony) has one King size bed and 1 sofabed and is recommended for you to enjoy the place in the most comfortable and romantic way.</p>\r\n<p>The bungalows are located right next to the stream which is really relaxing and it give the feeling like you&rsquo;re sleeping under the rain on a cozy bed.</p>\r\n<p><strong>Amenities:</strong> <em>Balcony, chairs, table, free coffee/tea and 2 water bottles/day, fragrance oil, candles, fresh flowers, ceiling fan, lamps, leaf fans, conical hats, mini fridge, sofabed, books, etc.</em></p><p>In order to remain the traditional design of Thai people, the sanitary block was built outside and in a common space with 2 bathrooms and 2 toilets artfully built of stone, wood and latanier leaves.</p>', '<p>Chaque bungalow (30 m2 dont la terresse de 10 m2), &eacute;quip&eacute; d&rsquo;un lit King size et un extra-bed, peut accueillir 3 personnes. Recommand&eacute; pour profiter vos s&eacute;jours d&rsquo;une fa&ccedil;on la plus confortable et romantique.</p>\r\n<p>Les bungalows se situ&eacute;s juste &agrave; c&ocirc;t&eacute; du ruisseau qui est vraiment relaxant en donnant l\'impression de dormir sous la pluie sur un lit confortable.</p>\r\n<p><strong>Facilit&eacute;s:</strong> <em>Balcon, chaises, table, caf&eacute; / th&eacute; gratuit et 2 bouteilles d\'eau / jour, huile de parfum, bougies, fleurs fra&icirc;ches, ventilateur de plafond, lampes, ventilateurs de feuilles, chapeaux coniques, mini r&eacute;frig&eacute;rateur, canap&eacute;-lit, livres, etc.</em></p><p>Afin de maintenir la conception traditionnelle des Tha&iuml;s, le bloc sanitaire est construit &agrave; l&rsquo;ext&eacute;rieur et dans un espace commun avec 2 salles de bain et 2 toilettes esth&eacute;tiquement construites en pierre, bois et feuilles de latanier.</p>', '2017-01-10 06:58:52', '2017-01-10 06:58:56', 'Bungalows', 'Bungalows', '/images/image_bt2.png', '/images/slide.png');

-- ----------------------------
-- Table structure for home
-- ----------------------------
DROP TABLE IF EXISTS `home`;
CREATE TABLE `home` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `body_en` text COLLATE utf8_unicode_ci NOT NULL,
  `body_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of home
-- ----------------------------
INSERT INTO `home` VALUES ('1', 'Pu Luong Nature Reserve, 200 km from Hanoi, has an evident touristic value thanks to its\r\n\r\nprimary forests, its waterfalls, its terraced rice fields and Thais villages and this is a great\r\n\r\ndestination for a few days trips from Hanoi.', 'Le charmant et confortable lodge Ban Hiêu Garden au village de Hiêu se situe dans la réserve\r\n\r\nnaturelle de Pu Luong dans la province de Thanh Hoa, à environ 200 km de Hanoi ayant une\r\n\r\nvaleur touristique indiscutable avec ses forêts primaires, ses cascades, ses rizières en terrasse\r\n\r\net des villages Thais. Les traditions Thai, la convivialité et le professionnalisme des locaux vous\r\n\r\nassurent un séjour de qualité.', null, null);

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `route` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `weight` tinyint(4) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', 'lodge', 'Our Lodge', '', 'Notre Lodge', null, null, null, '1');
INSERT INTO `menus` VALUES ('2', 'detail', 'Our Rooms', 'Giới thiệu', 'Nos chambres', null, null, null, '1');
INSERT INTO `menus` VALUES ('3', 'gallery', 'Gallery', 'Thư viên', 'Gallerie', null, null, null, '1');
INSERT INTO `menus` VALUES ('4', 'contact', 'Contact', 'Liên hệ', 'Contact', null, null, null, '1');
INSERT INTO `menus` VALUES ('5', 'home', 'Home', 'Trang chủ', 'Home', null, null, null, '1');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2017_01_05_033124_create_menus_table', '1');
INSERT INTO `migrations` VALUES ('4', '2017_01_05_045002_create_systems_table', '2');
INSERT INTO `migrations` VALUES ('5', '2017_01_05_045018_create_roms_table', '2');
INSERT INTO `migrations` VALUES ('6', '2017_01_09_223146_create_table_contacts', '2');
INSERT INTO `migrations` VALUES ('7', '2017_01_09_233304_create_table_home', '3');
INSERT INTO `migrations` VALUES ('8', '2017_01_09_233332_create_table_detail', '3');
INSERT INTO `migrations` VALUES ('9', '2017_01_09_233357_create_table_about', '3');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`) USING BTREE,
  KEY `password_resets_token_index` (`token`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for roms
-- ----------------------------
DROP TABLE IF EXISTS `roms`;
CREATE TABLE `roms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `description_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of roms
-- ----------------------------

-- ----------------------------
-- Table structure for systems
-- ----------------------------
DROP TABLE IF EXISTS `systems`;
CREATE TABLE `systems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `footer_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `footer_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `favicon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of systems
-- ----------------------------
INSERT INTO `systems` VALUES ('1', 'BAN HIEU GARDEN LODGE', 'BAN HIEU GARDEN LODGE', 'BAN HIEU GARDEN LODGE', 'BAN HIEU GARDEN LODGE', 'BAN HIEU GARDEN LODGE', 'BAN HIEU GARDEN LODGE', '', 'banhieugarden@gmail.com', '+84 983 362 787', '+84 904 298 536 ', '2017-01-10 05:50:20', '2017-01-10 05:50:17');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Admin', 'admin@banhieugarden.com', '$2y$10$ADYQh1jQW6uQi7EFjnrAkOmjjfiw9oo0HoarOhOO413.qT.DEFixW', null, '2017-02-08 04:20:00', '2017-02-08 04:20:00');

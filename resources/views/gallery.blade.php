@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="css/blueimp-gallery.css">
    <link rel="stylesheet" href="css/blueimp-gallery-indicator.css">
    <link rel="stylesheet" href="css/blueimp-gallery-video.css">
@endsection

@section('content')
    <div class="container">
        <div class="wrapper">
            <div class="wrap-content">
                <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                <div id="blueimp-gallery" class="blueimp-gallery">
                    <div class="slides"></div>
                    <h3 class="title"></h3>
                    <a class="prev">‹</a>
                    <a class="next">›</a>
                    <a class="close">×</a>
                    <a class="play-pause"></a>
                    <ol class="indicator"></ol>
                </div>
                <div id="links">
                </div>
                <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-carousel">
                    <div class="slides"></div>
                    <h3 class="title"></h3>
                    <a class="prev">‹</a>
                    <a class="next">›</a>
                    <a class="play-pause"></a>
                    <ol class="indicator"></ol>
                </div>
            </div>
        </div><!-- /primary -->
    </div>

@endsection


@section('scripts')
    <script src="js/blueimp-helper.js"></script>
    <script src="js/blueimp-gallery.js"></script>
    <script src="js/blueimp-gallery-fullscreen.js"></script>
    <script src="js/blueimp-gallery-indicator.js"></script>
    <script src="js/blueimp-gallery-video.js"></script>
    <script src="js/blueimp-gallery-vimeo.js"></script>
    <script src="js/blueimp-gallery-youtube.js"></script>
    <script src="js/vendor/jquery.js"></script>
    <script src="js/jquery.blueimp-gallery.js"></script>
@endsection

@section('inline_script')
    <script>
        $(function () {
            'use strict'
            $.ajax({
                url: '{{route('ajax')}}',
            }).done(function (result) {
                var carouselLinks = []
                var linksContainer = $('#links')
                var baseUrl
                // Add the demo images as links with thumbnails to the page:
                console.log(result)
                $.each(result, function (index, photo) {
                    $('<a/>')
                            .append($('<img>').prop('src', photo.thumbnail))
                            .prop('href', photo.url)
                            .prop('title', photo.title)
                            .attr('data-gallery', '')
                            .appendTo(linksContainer)
                    carouselLinks.push({
                        href: photo.url,
                        title: photo.title
                    })
                })
                console.log(carouselLinks)
                // Initialize the Gallery as image carousel:
                blueimp.Gallery(carouselLinks, {
                    container: '#blueimp-image-carousel',
                    carousel: true
                })
            })

//            blueimp.Gallery(carouselLinks, {
//                container: '#blueimp-image-carousel',
//                carousel: true
//            });

            // Initialize the Gallery as video carousel:
            blueimp.Gallery([
                {
                    title: 'Sintel',
                    href: 'https://archive.org/download/Sintel/sintel-2048-surround_512kb.mp4',
                    type: 'video/mp4',
                    poster: 'https://i.imgur.com/MUSw4Zu.jpg'
                },
                {
                    title: 'Big Buck Bunny',
                    href: 'https://upload.wikimedia.org/wikipedia/commons/7/75/' +
                    'Big_Buck_Bunny_Trailer_400p.ogg',
                    type: 'video/ogg',
                    poster: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/' +
                    'Big.Buck.Bunny.-.Opening.Screen.png/' +
                    '800px-Big.Buck.Bunny.-.Opening.Screen.png'
                },
                {
                    title: 'Elephants Dream',
                    href: 'https://upload.wikimedia.org/wikipedia/commons/transcoded/8/83/' +
                    'Elephants_Dream_%28high_quality%29.ogv/' +
                    'Elephants_Dream_%28high_quality%29.ogv.360p.webm',
                    type: 'video/webm',
                    poster: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/' +
                    'Elephants_Dream_s1_proog.jpg/800px-Elephants_Dream_s1_proog.jpg'
                },
                {
                    title: 'LES TWINS - An Industry Ahead',
                    type: 'text/html',
                    youtube: 'zi4CIXpx7Bg'
                },
                {
                    title: 'KN1GHT - Last Moon',
                    type: 'text/html',
                    vimeo: '73686146',
                    poster: 'https://secure-a.vimeocdn.com/ts/448/835/448835699_960.jpg'
                }
            ], {
                container: '#blueimp-video-carousel',
                carousel: true
            })
        })
    </script>
@endsection

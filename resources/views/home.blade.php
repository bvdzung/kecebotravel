@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="banner">
            <div id="owl-carousel" class="owl-carousel" data-index="0">
                @foreach($images as $image)
                    <div class="item">
                        <img src="{{$image->path}}" alt="Slide 1" class="img-responsive"
                             aria-describedby="slick-slide1">
                    </div>
                @endforeach
            </div>
            <div class="box-meta">
                <div class="box-meta-content">
                    <div class="item-head">
                        {{--<h1 class="item-title">Room A</h1>--}}
                    </div>
                    <div class="item-desc">
                        <p>{{$home->getAttributeWithLocale('body')}}</p>
                    </div>
                    <div class="bton">
                        <a href="{{route('lodge')}}" class="btn btn-default navbar-btn">Detail</a>
                    </div>
                </div>
            </div>
        </div><!-- /primary -->
    </div>
    <div class="desc">
        <div class="container">
            <p>{{$system->getAttributeWithLocale('footer')}}</p>
        </div>
    </div><!-- /desc -->

@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="wrapper">

            <div class="thumbnail-slider images">
                <div class="thumbnail-nav">
                    @foreach($details as $detail)
                        <a href="{{route('changeType', ['type' => $detail->type])}}">
                            <div class="item-img {{$detail->type == $type ? "t-active" : ""}}">
                                <img src="{{$images->where('type', $detail->type == 1 ?  3 : 5)->first()->path}}" alt="Slide 1" class="img-responsive">
                                <h1 class="h1-s">{{$detail->getAttributeWithLocale('title')}}</h1>
                            </div>
                        </a>
                    @endforeach
                </div>
                <div class="thumbnail-main">
                    <div id="owl-carousel1" class="owl-carousel" data-index="0">
                        @foreach($images->where('type', $type == 1 ?  2 : 4) as $image)
                            <div class="item">
                                <img src="{{$image->path}}" alt="Slide 1"
                                     aria-describedby="slick-slide1" class="img-responsive">
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>

            <div class="wrap-content">
                <div class="content">
                    <h2>{{$detail->where('type', $type)->first()->getAttributeWithLocale('title')}}</h2>
                    {!!  $detail->where('type', $type)->first()->getAttributeWithLocale('body')!!}
                </div>
            </div>
        </div>

    </div>
@endsection



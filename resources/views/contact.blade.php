@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="wrapper">
            <div class="wrap-content">
                {!! $contact->getAttributeWithLocale('body') !!}
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d955006.2565778602!2d104.8079402!3d20.7713262!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313402e8ae1a5b51%3A0xd53f19523eb02575!2zQuG6o24gSGnDqnU!5e0!3m2!1sen!2s!4v1485197761460"
                        width="100%" height="500px"
                        frameborder="0" style="border:0" allowfullscreen>

                </iframe>
            </div>
        </div><!-- /primary -->
    </div>

@endsection

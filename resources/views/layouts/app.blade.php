<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$system->title_en}}</title>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/assets/js/bootstrap/bootstrap.min.js"></script>

    <script src="/assets/js/owl.carousel.js"></script>
    <link rel="stylesheet" href="assets/css/owl.carousel.css">

    <script src="/assets/js/custom.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <link href="/assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    @yield('styles')
    <link rel="stylesheet" href="/assets/css/reset.css">
    <link rel="stylesheet" href="/assets/css/styles.css">
</head>
<body>
<div class="site">

    <header id="header">


        <div class="topbar">
            <div class="container">
                <ul class="menu-meta">
                    <li>Email: <a href="mailto:webdesign@arena.edu.vn">{{$system->email}}</a></li>
                    <li>Phone: <a href="tel:{{str_replace(' ', '', $system->phone)}}">{{$system->phone}}</a></li>
                    <li>Mobile: <a href="tel:{{str_replace(' ', '', $system->mobile)}}">{{$system->mobile}}</a></li>
                </ul>
            </div>
        </div><!-- /topbar -->

        <nav class="navbar">
            <div class="container-fluid">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            @foreach($menus as $menu)
                                <li class="{{Route::currentRouteName() == $menu->route ? 'active' : ""}}">
                                    <a href="{{route($menu->route)}}">{{$menu->getAttributeWithLocale("title")}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                        <ul class="lang">
                            <li><a href="{{route('changeLanguage', ['language' => 'en'])}}">E</a></li>
                            <li><a href="{{route('changeLanguage', ['language' => 'fr'])}}">F</a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div><!-- /.container -->
            </div>
        </nav>
        @if(Route::currentRouteName() != 'home')
            <div class="banner-df">
                <div class="parallax"
                     style="background-image: url(/assets/images/banner_df.png); min-height: 150px; background-size: cover;"></div>
            </div>
        @endif


    </header><!-- /header -->

    <div id="primary" class="content-area">
        @yield('content')
    </div><!-- /primary -->

    <footer class="wrap-footer">

    </footer>

</div><!-- /site -->
@yield('scripts')
@yield('inline_script')
</body>
</html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin Manage</title>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/assets/js/bootstrap/bootstrap.min.js"></script>

    <script src="/assets/js/owl.carousel.js"></script>
    <link rel="stylesheet" href="assets/css/owl.carousel.css">

    <script src="/assets/js/custom.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <link href="/assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!-- Include Editor style. -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">

    <!-- Include Editor style. -->
    <link href="/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css"/>
    <link href="/css/froala_style.min.css" rel="stylesheet" type="text/css"/>

    <!-- Include JS file. -->
    <link rel="stylesheet" href="/assets/css/reset.css">
    <link rel="stylesheet" href="/assets/css/styles.css">

    <!-- Include external JS libs. -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>

    <!-- Include JS files. -->
    <script type="text/javascript" src="/js/froala_editor.pkgd.min.js"></script>

    <!-- Include Language file if we want to use it. -->
    <script type="text/javascript" src="/js/languages/ro.js"></script>
    <script type='text/javascript'
            src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.2/js/froala_editor.min.js'></script>
    @yield('styles')
</head>
<body>
<div class="container">

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="col-lg-10">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">BAN HIEU GARDEN LODGE AMIN</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Manage Web
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{route('admin.home')}}">Home</a></li>
                                <li><a href="{{route('admin.contact')}}">Contact</a></li>
                                <li><a href="{{route('admin.lodge')}}">Lodge</a></li>
                                <li><a href="{{route('admin.menu')}}">Menu</a></li>
                                <li><a href="{{route('admin.image.index')}}">Image</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Config
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{route('admin.system')}}">System</a></li>
                                <li><a href="{{route('admin.user')}}">User</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
            <div class="col-lg-2">
                <div class="pull-right">
                    <p>Welcome: {{Auth::user()->name}}</p>
                    <p>
                    <form method="post" action="{{route('logout')}}">
                        {{ csrf_field() }}
                        <button type="submit">Logout</button>
                    </form>
                    </p>
                </div>
            </div>
        </div><!--/.container-fluid -->
    </nav>

    <div>
        @yield('content')
    </div>

</div><!-- /site -->
@yield('scripts')
@yield('inline_script')
</body>
</html>
@extends('layouts.admin')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success" role="alert">{{session('success')}}</div>
            @endif
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Email</th>
                    <th>Name</th>
                    <th>Created At</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$loop->index + 1}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->created_at}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-12">
                <a href="{{route('admin.userRegister')}}"><button type="submit" class="btn btn-warning"><i class="fa fa-plus"></i> Add User</button></a>
            </div>
        </div>
    </div>
@endsection
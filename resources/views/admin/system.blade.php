@extends('layouts.admin')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success" role="alert">{{session('success')}}</div>
            @endif
            {{Form::open(['route' => 'admin.updateMenu', 'method' => 'post'])}}
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Title English</label>
                        {{ Form::text('title_en', $system->title_en, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Title France</label>
                        {{ Form::text('title_fr', $system->title_fr, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Description English</label>
                        {{ Form::text('description_en', $system->description_en, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Description France</label>
                        {{ Form::text('description_fr', $system->description_fr, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Footer English</label>
                        {{ Form::text('footer_en', $system->footer_en, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Footer France</label>
                        {{ Form::text('footer_fr', $system->footer_fr, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        {{ Form::text('email', $system->email, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Phone</label>
                        {{ Form::text('phone', $system->phone, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mobile</label>
                        {{ Form::text('mobile', $system->mobile, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="col-md-12">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
@endsection
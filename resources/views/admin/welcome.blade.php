@extends('layouts.admin')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <p>Welcome {{Auth::user()->name}} to Manage Website!</p>
            <p>Have a nice day</p>
        </div>
    </div>
@endsection

@section('scripts')
@endsection
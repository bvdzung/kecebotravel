@extends('layouts.admin')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success" role="alert">{{session('success')}}</div>
            @endif
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Thumbnail</th>
                    <th>Type</th>
                    <th>Created At</th>
                    <th>Remove</th>
                </tr>
                </thead>
                <tbody>
                @foreach($images as $image)
                    <tr>
                        <td>{{$loop->index + 1}}</td>
                        <td><img src="{{$image->thumbnail}}"></td>
                        <td>{{config('image.type')[$image->type]}}</td>
                        <td>{{$image->created_at}}</td>
                        <td>
                            <a href="{{route('admin.image.remove', ['id' => $image->id])}}">
                                <button class="btn btn-warning"><i class="fa fa-remove"></i> Delete</button>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-12">
                <a href="{{route('admin.image.add')}}">
                    <button type="submit" class="btn btn-warning"><i class="fa fa-plus"></i> Add Imgae</button>
                </a>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
    </script>
@endsection
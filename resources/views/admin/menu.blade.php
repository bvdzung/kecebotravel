@extends('layouts.admin')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success" role="alert">{{session('success')}}</div>
            @endif
            {{Form::open(['route' => 'admin.updateMenu', 'method' => 'post'])}}
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Name</th>
                    <th>Weight</th>
                    <th>Active</th>
                </tr>
                </thead>
                <tbody>
                @foreach($menus as $menu)
                    <tr>
                        <td>{{$loop->index + 1}}{{ Form::hidden("data[$menu->id][id]", $menu->id) }}</td>
                        <td>{{$menu->title_en}}</td>
                        <td>
                            <div class="form-group">
                                {{ Form::number("data[$menu->id][weight]", $menu->weight, ['class' => 'form-control']) }}
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                {{ Form::select("data[$menu->id][active]", [0 => "Inactive", 1 => 'Active'], $menu->active, ['class' => 'form-control']) }}
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-12">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
            {{Form::close()}}
        </div>
    </div>
@endsection
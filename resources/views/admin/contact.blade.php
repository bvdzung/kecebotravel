@extends('layouts.admin')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success" role="alert">{{session('success')}}</div>
            @endif
            {{Form::open(['route' => 'admin.updateContact', 'method' => 'post'])}}
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Body English</label>
                        {{ Form::textarea('body_en', $contact->body_en, ['class' => 'editor']) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Body Fance</label>
                        {{ Form::textarea('body_fr', $contact->body_fr, ['class' => 'editor']) }}
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $('.editor').froalaEditor({
//                direction: 'rtl'
            })
        });
    </script>
@endsection
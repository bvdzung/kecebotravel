@extends('layouts.admin')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success" role="alert">{{session('success')}}</div>
            @endif
            {{Form::open(['route' => 'admin.updateHome', 'method' => 'post'])}}
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Body English</label>
                        {{ Form::textarea('body_en', $home->body_en, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Body Fance</label>
                        {{ Form::textarea('body_fr', $home->body_fr, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('scripts')
@endsection
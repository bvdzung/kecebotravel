@extends('layouts.app')



@section('content')
    <div class="container">
        <div class="wrapper">

            <div class="thumbnail">
                <img src="{{$image->path}}" alt="Giới Thiệu" class="img-responsive">
            </div>

            <div class="wrap-content">
                <h2>{{$about->getAttributeWithLocale('title')}}</h2>
                {!!  $about->getAttributeWithLocale('body')!!}
            </div>
        </div>

    </div>
@endsection



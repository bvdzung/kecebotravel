-- MySQL dump 10.13  Distrib 5.5.52, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: pet-funny
-- ------------------------------------------------------
-- Server version	5.5.52-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `pet-funny`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `pet-funny` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `pet-funny`;

--
-- Table structure for table `about`
--

DROP TABLE IF EXISTS `about`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `about` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `body_en` text COLLATE utf8_unicode_ci NOT NULL,
  `body_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tile_fr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `about`
--

LOCK TABLES `about` WRITE;
/*!40000 ALTER TABLE `about` DISABLE KEYS */;
INSERT INTO `about` VALUES (1,'<p style=\"text-align: justify;\">Pu Luong Nature Reserve, 200 km from Hanoi, has an evident touristic value thanks to its primary forests, its waterfalls, its terraced rice fields and Thais villages and this is a great destination for a few days trips from Hanoi.</p><p style=\"text-align: justify;\">Ban Hiêu Garden Logde is situated in Hieu village – in the core of Pu Luong Nature Reserve – where you can find yourself in the middle of a natural wild forest, emerging in the cool water of Hieu waterfalls et admiring the breathtaking terraced rice fields while staying with Thai.</p><p style=\"text-align: justify;\">The lodge is&nbsp;located in the moutainside, lovely wedged between two waterfalls – a hidden germ to explore. The Thai traditions, the friendly atmosphere and the professionalism of local people guarantee a pleasant tonkin stay.</p><p style=\"text-align: justify;\">The best seasons to visit Ban Hieu is from September to November or February until May when the yellow rice fields &nbsp;before harvest show an amazing view before you and the weather is pleasant.</p><p style=\"text-align: justify;\"><em><strong>What to explore around Ban Hieu Garden Lodge?</strong></em></p><ul style=\"list-style-type: square; text-align: justify;\"><li>The Hieu waterfall is just 50m from our lodge where you can find a stunning natural pool – suitable for swimming. You can also walk to the upstream to discover even more waterfalls.</li><li>There are lots of place to discover by trekking or on motorcycle:<ul><li style=\"text-align: justify;\">Visit Pho Doan market with lots of local products (only in the morning).</li><li style=\"text-align: justify;\">Hieu village (original) on top of the mountain with a panorama view to the valley.</li><li style=\"text-align: justify;\">Visit Khuyn village – 5km from Hieu village or Kho Muong – 5h trekking.</li><li style=\"text-align: justify;\">Visit Son – Ba – Muoi, 3 villages in the neighborhood. On the way, you will surely be amazed by the breathtaking view to the mountains and valleys of Pu Luong.</li></ul></li></ul><p style=\"text-align: justify;\"><em><strong>How to get there?</strong></em></p><p style=\"text-align: justify;\">Ban Hieu Garden Lodge is located at Ban Hieu, Ba Thuoc, Thanh Hoa Province. From Hanoi, please follow the Highway Lang - Hoa Lac (or through Ha Dong) and keep up on Ho Chi Minh Highway to Cam Thuy (Thanh Hoa) - about 4 hours driving. Then from Cam Thuy to Canh Nang town - Pho Doan.</p><p style=\"text-align: justify;\">From here, you must leave your car and walk about 5 km on a trail to Ban Hieu. The roadside scene is amazing with terrace rice fields, streams, small waterfalls, etc. If walking is not your thing, then you can order motortaxi in Pho Doan for 15min to Ban Hieu. Or you travel on a motobike, it would be a very interesting route to explore ‘till the gate of our lodge.</p><p><iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d955006.2565778602!2d104.8079402!3d20.7713262!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313402e8ae1a5b51%3A0xd53f19523eb02575!2zQuG6o24gSGnDqnU!5e0!3m2!1sen!2s!4v1485197761460\" width=\"100%\" height=\"500px\" frameborder=\"0\" style=\"border:0\" allowfullscreen=\"\">&amp;amp;nbsp;</iframe></p>','<p style=\"text-align: justify;\">Le charmant et confortable lodge Ban Hiêu Garden au village de Hiêu se situe dans la réserve naturelle de Pu Luong dans la province de Thanh Hoa, à environ 200 km de Hanoi ayant une valeur touristique indiscutable avec ses forêts primaires, ses cascades, ses rizières en terrasse et des villages Thais. Les traditions Thai, la convivialité et le professionnalisme des locaux vous assurent un séjour de qualité.</p><p style=\"text-align: justify;\">Ban Hieu Garden Lodge est situé à flanc de montage, joliment enclavé entre deux chutes d’eau avec une maison sur pilotis et 2 bungalows.</p><p style=\"text-align: justify;\">La meilleure saison pour visiter Ban Hieu est du mois de septembre au mois de novembre ou du mois de février jusqu’au mois de mai où les champs de riz jaunes avant la récolte montrent une vue étonnante et le temps est très agréable.</p><p style=\"text-align: justify;\"><em><strong>À découvrir:</strong></em></p><ul style=\"list-style-type: circle; text-align: justify;\"><li>Les cascades de Hieu sont juste 50m de notre lodge où vous trouverez une piscine naturelle magnifique. Vous pourriez marcher à l’amont pour découvrir plus de chutes d’eau.</li><li>Plusieurs de lieux à découvrir en trekking ou en moto:<ul><li>Le marché de Pho Doan (10km de Ban Hieu Garden Lodge) avec ses produits locaux (matinée)</li><li>Le village de Hieu (original) au sommet de la montage donnant une vue panoramique sur la vallée</li><li>Le village de Khuyn – 5km de Ban Hieu ou Kho Muong – 5h de trekking</li><li>Les villages Son-Ba- Muoi dans le voisinage. Sur le route, vous serez certainement étonnés par la vue impeccable sur les montages et la vallée de Pu Luong.</li></ul></li></ul><p style=\"text-align: justify;\"><em><strong>Comment y aller?</strong></em></p><p style=\"text-align: justify;\">Ban Hieu Garden Lodge se situe au village de Hieu, Ba Thuoc, Thanh Hoa. De Hanoi, suivez l’autoroute Lang – Hoa Lac (ou par Ha Dong) et continuez sur la route Hochiminh jusqu’à Cam Thuy (Thanh Hoa) – environ 4h de route et puis de Cam Thuy à la ville Canh Nang – Pho Doan. A partir de ce point, vous devriez laisser votre voiture et faire un trek léger (5km) pour arriver au village de Hieu. La scène sur la route est très jolie montrant des rizières en terrace, des ruisseaux, ascades, etc. Vous pourriez éventuellment demander un motor taxi (géré par les locaux).</p><p><iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d955006.2565778602!2d104.8079402!3d20.7713262!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313402e8ae1a5b51%3A0xd53f19523eb02575!2zQuG6o24gSGnDqnU!5e0!3m2!1sen!2s!4v1485197761460\" width=\"100%\" height=\"500px\" frameborder=\"0\" style=\"border:0\" allowfullscreen=\"\">&amp;nbsp;</iframe></p>',NULL,'2017-02-13 03:37:11',NULL,'À décrovrir:');
/*!40000 ALTER TABLE `about` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `body_en` text COLLATE utf8_unicode_ci NOT NULL,
  `body_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'<p><strong>BAN HIEU GARDEN LODGE</strong></p>\r\n<p>Address : Hieu village, Cam Thuy Commune, Ba Thuoc District, Thanh Hoa City, Vietnam</p>\r\n<p>Tel : <a href=\"tel:+84904298536\">+84 904 298 536</a>&nbsp;/ <a href=\"tel:+84983362787\">+84 983 362 787</a>&nbsp;(English, French, Vietnamese)</p>\r\n<p>Email :<a href=\"mailto:banhieugarden@gmail.com\">banhieugarden@gmail.com</a></p>\r\n<p>Facebook : <a title=\"https://www.facebook.com/Ban-Hieu- Garden-Lodge-  535151283327796/?ref=aymt_homepage_panel\" href=\"https://www.facebook.com/Ban-Hieu-%20Garden-Lodge-  535151283327796/?ref=aymt_homepage_panel\">https://www.facebook.com/Ban-Hieu- Garden-Lodge-</a></p>\r\n<p><a title=\"https://www.facebook.com/Ban-Hieu- Garden-Lodge-  535151283327796/?ref=aymt_homepage_panel\" href=\"https://www.facebook.com/Ban-Hieu-%20Garden-Lodge-  535151283327796/?ref=aymt_homepage_panel\">535151283327796/?ref=aymt_homepage_panel</a></p>\r\n<p>Flickr :&nbsp;<a href=\"https://www.flickr.com/photos/134819256@N02/\">https://www.flickr.com/photos/134819256@N02/</a></p>','<p><strong>BAN HIEU GARDEN LODGE</strong></p>\r\n<p>Address : Hieu village, Cam Thuy Commune, Ba Thuoc District, Thanh Hoa City, Vietnam</p>\r\n<p>T&eacute;l&nbsp;: <a href=\"tel:+84904298536\">+84 904 298 536</a>&nbsp;/ <a href=\"tel:+84983362787\">+84 983 362 787</a>&nbsp;(English, French, Vietnamese)</p>\r\n<p>Email :<a href=\"mailto:banhieugarden@gmail.com\">banhieugarden@gmail.com</a></p>\r\n<p>Facebook : <a title=\"https://www.facebook.com/Ban-Hieu- Garden-Lodge-  535151283327796/?ref=aymt_homepage_panel\" href=\"https://www.facebook.com/Ban-Hieu-%20Garden-Lodge-  535151283327796/?ref=aymt_homepage_panel\">https://www.facebook.com/Ban-Hieu- Garden-Lodge-</a></p>\r\n<p><a title=\"https://www.facebook.com/Ban-Hieu- Garden-Lodge-  535151283327796/?ref=aymt_homepage_panel\" href=\"https://www.facebook.com/Ban-Hieu-%20Garden-Lodge-  535151283327796/?ref=aymt_homepage_panel\">535151283327796/?ref=aymt_homepage_panel</a></p>\r\n<p>Flickr :&nbsp;<a href=\"https://www.flickr.com/photos/134819256@N02/\">https://www.flickr.com/photos/134819256@N02/</a></p>','2017-01-10 05:42:10','2017-01-10 05:42:13');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detail`
--

DROP TABLE IF EXISTS `detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `body_en` text COLLATE utf8_unicode_ci NOT NULL,
  `body_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detail`
--

LOCK TABLES `detail` WRITE;
/*!40000 ALTER TABLE `detail` DISABLE KEYS */;
INSERT INTO `detail` VALUES (1,'<p>The stilt house was built with tradition Thai&rsquo;s structure and perfectly designed for a group from 4 up to 8-10 pax with 2 private Double rooms, 1 Double bed and 2 Single mattresses (1.2m x 2m) in open space. All mattresses used are Dunlopillo with qualified draps and pillows toensure your best sleep.</p>\r\n<p><strong>Amenities:</strong> <em>Chais, table, fresh flowers, Games (Dart, Chess, Poker, etc. and others games for children), Bookshelf, Books in Vietnamese and other languages, trash can, hangers, fan, ceiling fan, lamps, leaf fans, conical hats, etc.</em></p><p>In order to remain the traditional design of Thai people, the sanitary block was built outside and in a common space with 2 bathrooms and 2 toilets artfully built of stone, wood and latanier leaves.</p>\r\n','<p>La maison sur pilotis a &eacute;t&eacute; construite avec la structure tha&iuml;landaise traditionnelle et parfaitement con&ccedil;ue pour un groupe de 4 &agrave; 8-10 pax avec 2 chambres doubles priv&eacute;es, 1 lit double et 2 matelas simples (1.2m x 2m) en plein air. Tous les matelas utilis&eacute;s sont Dunlopillo avec draps et oreillers qualifi&eacute;s pour assurer votre meilleur sommeil.</p>\r\n<p><strong>Facilit&eacute;s:</strong> <em>Chais, table, fleurs fra&icirc;ches, Jeux (Dart, Chess, Poker, etc. et autres jeux pour enfants), &eacute;tag&egrave;re, Livres en vietnamien et autres langues, poubelles, Chapeaux coniques, etc.</em></p><p>Afin de maintenir la conception traditionnelle des Tha&iuml;s, le bloc sanitaire est construit &agrave; l&rsquo;ext&eacute;rieur et dans un espace commun avec 2 salles de bain et 2 toilettes esth&eacute;tiquement construites en pierre, bois et feuilles de latanier.</p>','2017-01-10 06:57:39','2017-01-10 06:57:42','Stilt house','Maison sur pilotis','/images/image_bt1.png','/images/slide.png',1),(2,'<p>Each bungalow (30m2 including the balcony) has one King size bed and 1 sofabed and is recommended for you to enjoy the place in the most comfortable and romantic way.</p>\r\n<p>The bungalows are located right next to the stream which is really relaxing and it give the feeling like you&rsquo;re sleeping under the rain on a cozy bed.</p>\r\n<p><strong>Amenities:</strong> <em>Balcony, chairs, table, free coffee/tea and 2 water bottles/day, fragrance oil, candles, fresh flowers, ceiling fan, lamps, leaf fans, conical hats, mini fridge, sofabed, books, etc.</em></p><p>In order to remain the traditional design of Thai people, the sanitary block was built outside and in a common space with 2 bathrooms and 2 toilets artfully built of stone, wood and latanier leaves.</p>','<p>Chaque bungalow (30 m2 dont la terresse de 10 m2), &eacute;quip&eacute; d&rsquo;un lit King size et un extra-bed, peut accueillir 3 personnes. Recommand&eacute; pour profiter vos s&eacute;jours d&rsquo;une fa&ccedil;on la plus confortable et romantique.</p>\r\n<p>Les bungalows se situ&eacute;s juste &agrave; c&ocirc;t&eacute; du ruisseau qui est vraiment relaxant en donnant l\'impression de dormir sous la pluie sur un lit confortable.</p>\r\n<p><strong>Facilit&eacute;s:</strong> <em>Balcon, chaises, table, caf&eacute; / th&eacute; gratuit et 2 bouteilles d\'eau / jour, huile de parfum, bougies, fleurs fra&icirc;ches, ventilateur de plafond, lampes, ventilateurs de feuilles, chapeaux coniques, mini r&eacute;frig&eacute;rateur, canap&eacute;-lit, livres, etc.</em></p><p>Afin de maintenir la conception traditionnelle des Tha&iuml;s, le bloc sanitaire est construit &agrave; l&rsquo;ext&eacute;rieur et dans un espace commun avec 2 salles de bain et 2 toilettes esth&eacute;tiquement construites en pierre, bois et feuilles de latanier.</p>','2017-01-10 06:58:52','2017-01-10 06:58:56','Bungalows','Bungalows','/images/image_bt2.png','/images/slide.png',2);
/*!40000 ALTER TABLE `detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home`
--

DROP TABLE IF EXISTS `home`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `home` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `body_en` text COLLATE utf8_unicode_ci NOT NULL,
  `body_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home`
--

LOCK TABLES `home` WRITE;
/*!40000 ALTER TABLE `home` DISABLE KEYS */;
INSERT INTO `home` VALUES (1,'Pu Luong Nature Reserve, 200 km from Hanoi, has an evident touristic value thanks to its\r\n\r\nprimary forests, its waterfalls, its terraced rice fields and Thais villages and this is a great\r\n\r\ndestination for a few days trips from Hanoi.','Le charmant et confortable lodge Ban Hiêu Garden au village de Hiêu se situe dans la réserve\r\n\r\nnaturelle de Pu Luong dans la province de Thanh Hoa, à environ 200 km de Hanoi ayant une\r\n\r\nvaleur touristique indiscutable avec ses forêts primaires, ses cascades, ses rizières en terrasse\r\n\r\net des villages Thais. Les traditions Thai, la convivialité et le professionnalisme des locaux vous\r\n\r\nassurent un séjour de qualité.',NULL,NULL);
/*!40000 ALTER TABLE `home` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `path` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (8,6,'/media/KisOt8Hf5QvV1487518618.jpg','/media/thumbnail/KisOt8Hf5QvV1487518618.jpg','2017-02-19 15:36:59','2017-02-19 15:36:59'),(9,0,'/media/asMBifiCdTVH1487518806.jpg','/media/thumbnail/asMBifiCdTVH1487518806.jpg','2017-02-19 15:40:06','2017-02-19 15:40:06'),(10,0,'/media/lISpmOuwXIs51487518813.jpg','/media/thumbnail/lISpmOuwXIs51487518813.jpg','2017-02-19 15:40:13','2017-02-19 15:40:13'),(11,0,'/media/zAwAPo2t7jun1487518820.jpg','/media/thumbnail/zAwAPo2t7jun1487518820.jpg','2017-02-19 15:40:20','2017-02-19 15:40:20'),(12,1,'/images/gallery/Before harvest.jpg','/images/gallery/thumbnails/Before harvest.jpg','2017-02-19 15:36:59',NULL),(13,1,'/images/gallery/By the pond.jpg','/images/gallery/thumbnails/By the pond.jpg','2017-02-19 15:36:59',NULL),(14,1,'/images/gallery/Cristal Clear water.jpg','/images/gallery/thumbnails/Cristal Clear water.jpg','2017-02-19 15:36:59',NULL),(15,1,'/images/gallery/Early morning.jpg','/images/gallery/thumbnails/Early morning.jpg','2017-02-19 15:36:59',NULL),(16,1,'/images/gallery/Hanging bed.jpg','/images/gallery/thumbnails/Hanging bed.jpg','2017-02-19 15:36:59',NULL),(17,1,'/images/gallery/Hieu Garden 2.jpg','/images/gallery/thumbnails/Hieu Garden 2.jpg','2017-02-19 15:36:59',NULL),(18,1,'/images/gallery/Hieu Garden 3.jpg','/images/gallery/thumbnails/Hieu Garden 3.jpg','2017-02-19 15:36:59',NULL),(19,1,'/images/gallery/Hieu Garden.jpg','/images/gallery/thumbnails/Hieu Garden.jpg','2017-02-19 15:36:59',NULL),(20,1,'/images/gallery/Made from home.jpg','/images/gallery/thumbnails/Made from home.jpg','2017-02-19 15:36:59',NULL),(21,1,'/images/gallery/Natural pool.jpg','/images/gallery/thumbnails/Natural pool.jpg','2017-02-19 15:36:59',NULL),(22,1,'/images/gallery/Reception.jpg','/images/gallery/thumbnails/Reception.jpg','2017-02-19 15:36:59',NULL),(23,1,'/images/gallery/Rice field.jpg','/images/gallery/thumbnails/Rice field.jpg','2017-02-19 15:36:59',NULL),(24,1,'/images/gallery/Surroungding.jpg','/images/gallery/thumbnails/Surroungding.jpg','2017-02-19 15:36:59',NULL),(45,4,'/media/p1ALQiR8Dbtq1487862821.jpg','/media/thumbnail/p1ALQiR8Dbtq1487862821.jpg','2017-02-23 15:13:41','2017-02-23 15:13:41'),(46,4,'/media/tw0cOvgvuz7n1487862825.jpg','/media/thumbnail/tw0cOvgvuz7n1487862825.jpg','2017-02-23 15:13:45','2017-02-23 15:13:45'),(47,2,'/media/vbC3kRQIVNPZ1487862834.jpg','/media/thumbnail/vbC3kRQIVNPZ1487862834.jpg','2017-02-23 15:13:54','2017-02-23 15:13:54'),(48,2,'/media/67E0jkAH0TiU1487862841.jpg','/media/thumbnail/67E0jkAH0TiU1487862841.jpg','2017-02-23 15:14:01','2017-02-23 15:14:01'),(49,2,'/media/AJhnqJb5E0sR1487862846.jpg','/media/thumbnail/AJhnqJb5E0sR1487862846.jpg','2017-02-23 15:14:06','2017-02-23 15:14:06'),(50,2,'/media/qpR5kpW8WOSQ1487862851.jpg','/media/thumbnail/qpR5kpW8WOSQ1487862851.jpg','2017-02-23 15:14:11','2017-02-23 15:14:11'),(51,2,'/media/ZA8ZrHd6e9QU1487862855.jpg','/media/thumbnail/ZA8ZrHd6e9QU1487862855.jpg','2017-02-23 15:14:16','2017-02-23 15:14:16'),(52,3,'/media/WxZYLmysniLM1487862867.jpg','/media/thumbnail/WxZYLmysniLM1487862867.jpg','2017-02-23 15:14:27','2017-02-23 15:14:27'),(53,5,'/media/eOg69FyD73pc1487862874.jpg','/media/thumbnail/eOg69FyD73pc1487862874.jpg','2017-02-23 15:14:34','2017-02-23 15:14:34'),(56,4,'/media/zKPumIYsn3pj1488190115.jpg','/media/thumbnail/zKPumIYsn3pj1488190115.jpg','2017-02-27 10:08:35','2017-02-27 10:08:35'),(58,4,'/media/ez4WWaPatb3M1488190265.jpg','/media/thumbnail/ez4WWaPatb3M1488190265.jpg','2017-02-27 10:11:05','2017-02-27 10:11:05'),(63,0,'/media/6H1O4qbT6E2K1488190979.jpg','/media/thumbnail/6H1O4qbT6E2K1488190979.jpg','2017-02-27 10:22:59','2017-02-27 10:22:59'),(64,0,'/media/WmxirJRyRehW1488191347.jpg','/media/thumbnail/WmxirJRyRehW1488191347.jpg','2017-02-27 10:29:07','2017-02-27 10:29:07'),(65,1,'/media/uqYcSlnW2zbm1488442386.jpg','/media/thumbnail/uqYcSlnW2zbm1488442386.jpg','2017-03-02 08:13:06','2017-03-02 08:13:06'),(66,1,'/media/MuIcf4TqJJ6P1488443476.jpg','/media/thumbnail/MuIcf4TqJJ6P1488443476.jpg','2017-03-02 08:31:16','2017-03-02 08:31:16'),(67,1,'/media/BWKW7LdXG2gk1488521137.jpg','/media/thumbnail/BWKW7LdXG2gk1488521137.jpg','2017-03-03 06:05:38','2017-03-03 06:05:38'),(68,1,'/media/9M0F84LmVs5w1488521179.jpg','/media/thumbnail/9M0F84LmVs5w1488521179.jpg','2017-03-03 06:06:19','2017-03-03 06:06:19'),(69,1,'/media/UhEk89bYm8dR1488521236.jpg','/media/thumbnail/UhEk89bYm8dR1488521236.jpg','2017-03-03 06:07:16','2017-03-03 06:07:16'),(70,1,'/media/dR9J8BhIxdSJ1488521263.jpg','/media/thumbnail/dR9J8BhIxdSJ1488521263.jpg','2017-03-03 06:07:43','2017-03-03 06:07:43'),(71,1,'/media/YjgeI7YJKzvw1488521292.jpg','/media/thumbnail/YjgeI7YJKzvw1488521292.jpg','2017-03-03 06:08:12','2017-03-03 06:08:12'),(72,1,'/media/oPaWeczIlNgq1488521504.jpg','/media/thumbnail/oPaWeczIlNgq1488521504.jpg','2017-03-03 06:11:44','2017-03-03 06:11:44'),(76,1,'/media/NOFU4kvd1iIM1488521575.jpg','/media/thumbnail/NOFU4kvd1iIM1488521575.jpg','2017-03-03 06:12:56','2017-03-03 06:12:56'),(77,1,'/media/vZcyFz2xhd3a1488521602.jpg','/media/thumbnail/vZcyFz2xhd3a1488521602.jpg','2017-03-03 06:13:22','2017-03-03 06:13:22'),(78,1,'/media/lYwkeRbqlAYR1488521669.jpg','/media/thumbnail/lYwkeRbqlAYR1488521669.jpg','2017-03-03 06:14:29','2017-03-03 06:14:29'),(79,1,'/media/fxuayQWdTYvI1488521703.jpg','/media/thumbnail/fxuayQWdTYvI1488521703.jpg','2017-03-03 06:15:03','2017-03-03 06:15:03'),(80,1,'/media/T3cZ7R12MIVj1488521715.jpg','/media/thumbnail/T3cZ7R12MIVj1488521715.jpg','2017-03-03 06:15:15','2017-03-03 06:15:15'),(81,1,'/media/b1K7hMWKATSf1488521730.jpg','/media/thumbnail/b1K7hMWKATSf1488521730.jpg','2017-03-03 06:15:30','2017-03-03 06:15:30'),(82,1,'/media/tgNGozsu2BZo1488521815.jpg','/media/thumbnail/tgNGozsu2BZo1488521815.jpg','2017-03-03 06:16:55','2017-03-03 06:16:55'),(83,1,'/media/OH6HkaUFejEQ1488521841.jpg','/media/thumbnail/OH6HkaUFejEQ1488521841.jpg','2017-03-03 06:17:21','2017-03-03 06:17:21'),(84,1,'/media/9pfmbg9ShpKh1488521864.jpg','/media/thumbnail/9pfmbg9ShpKh1488521864.jpg','2017-03-03 06:17:44','2017-03-03 06:17:44'),(85,1,'/media/8MZlIm7m8P2T1489733213.jpg','/media/thumbnail/8MZlIm7m8P2T1489733213.jpg','2017-03-17 06:46:53','2017-03-17 06:46:53'),(86,1,'/media/ssQ8nmtjEkRu1489733246.jpg','/media/thumbnail/ssQ8nmtjEkRu1489733246.jpg','2017-03-17 06:47:26','2017-03-17 06:47:26'),(87,1,'/media/PNG35QQns6FZ1489733272.jpg','/media/thumbnail/PNG35QQns6FZ1489733272.jpg','2017-03-17 06:47:52','2017-03-17 06:47:52'),(88,1,'/media/QvGx8NBuyoWR1489733302.jpg','/media/thumbnail/QvGx8NBuyoWR1489733302.jpg','2017-03-17 06:48:23','2017-03-17 06:48:23'),(89,1,'/media/SlfdrhlwYfwW1489733912.jpg','/media/thumbnail/SlfdrhlwYfwW1489733912.jpg','2017-03-17 06:58:32','2017-03-17 06:58:32'),(90,1,'/media/EZuJImb6DT7H1489733926.jpg','/media/thumbnail/EZuJImb6DT7H1489733926.jpg','2017-03-17 06:58:46','2017-03-17 06:58:46'),(91,1,'/media/GrSauUcBP7qn1489733942.jpg','/media/thumbnail/GrSauUcBP7qn1489733942.jpg','2017-03-17 06:59:02','2017-03-17 06:59:02'),(92,1,'/media/BeT98UL5dciU1489733952.jpg','/media/thumbnail/BeT98UL5dciU1489733952.jpg','2017-03-17 06:59:12','2017-03-17 06:59:12'),(93,1,'/media/CQKHnwp2JNhs1489733970.jpg','/media/thumbnail/CQKHnwp2JNhs1489733970.jpg','2017-03-17 06:59:30','2017-03-17 06:59:30'),(94,1,'/media/a70P5Dv6d0aD1489733991.jpg','/media/thumbnail/a70P5Dv6d0aD1489733991.jpg','2017-03-17 06:59:51','2017-03-17 06:59:51'),(95,2,'/media/M40HcdiN44Db1494235654.jpg','/media/thumbnail/M40HcdiN44Db1494235654.jpg','2017-05-08 09:27:34','2017-05-08 09:27:34'),(96,1,'/media/CxQx7RCzfURB1496114412.jpg','/media/thumbnail/CxQx7RCzfURB1496114412.jpg','2017-05-30 03:20:12','2017-05-30 03:20:12'),(97,2,'/media/IEcv2NKtO2js1496114443.jpg','/media/thumbnail/IEcv2NKtO2js1496114443.jpg','2017-05-30 03:20:43','2017-05-30 03:20:43'),(98,1,'/media/pv1zJgvbs26f1496114610.jpg','/media/thumbnail/pv1zJgvbs26f1496114610.jpg','2017-05-30 03:23:30','2017-05-30 03:23:30'),(100,4,'/media/ipkxUoMtO6nI1496114747.jpg','/media/thumbnail/ipkxUoMtO6nI1496114747.jpg','2017-05-30 03:25:47','2017-05-30 03:25:47');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `route` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `weight` tinyint(4) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (0,'home','Home','Trang chủ','Home',NULL,'2017-02-13 04:48:40',1,1),(1,'lodge','Our Lodge','','Notre Lodge',NULL,'2017-02-13 04:48:40',2,1),(2,'detail','Our Rooms','Giới thiệu','Nos chambres',NULL,'2017-02-12 17:01:24',3,1),(3,'gallery','Gallery','Thư viên','Gallerie',NULL,'2017-02-12 17:01:24',4,1),(4,'contact','Contact','Liên hệ','Contact',NULL,'2017-02-12 17:01:24',5,1);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2017_01_05_033124_create_menus_table',1),(4,'2017_01_05_045002_create_systems_table',2),(5,'2017_01_05_045018_create_roms_table',2),(6,'2017_01_09_223146_create_table_contacts',2),(7,'2017_01_09_233304_create_table_home',3),(8,'2017_01_09_233332_create_table_detail',3),(9,'2017_01_09_233357_create_table_about',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`) USING BTREE,
  KEY `password_resets_token_index` (`token`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roms`
--

DROP TABLE IF EXISTS `roms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `description_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roms`
--

LOCK TABLES `roms` WRITE;
/*!40000 ALTER TABLE `roms` DISABLE KEYS */;
/*!40000 ALTER TABLE `roms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systems`
--

DROP TABLE IF EXISTS `systems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `footer_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `footer_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `favicon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systems`
--

LOCK TABLES `systems` WRITE;
/*!40000 ALTER TABLE `systems` DISABLE KEYS */;
INSERT INTO `systems` VALUES (1,'BAN HIEU GARDEN LODGE','BAN HIEU GARDEN LODGE','BAN HIEU GARDEN LODGE','BAN HIEU GARDEN LODGE','BAN HIEU GARDEN LODGE','BAN HIEU GARDEN LODGE','','banhieugarden@gmail.com','+84 983 362 787','+84 904 298 536 ','2017-01-10 05:50:20','2017-01-10 05:50:17');
/*!40000 ALTER TABLE `systems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','admin@banhieugarden.com','$2y$10$ADYQh1jQW6uQi7EFjnrAkOmjjfiw9oo0HoarOhOO413.qT.DEFixW','PqotTmG63vQLv36LKFio6zlqggjk2Ze7fanpNzA54vk3y2IFObCDKptPQWMM','2017-02-08 04:20:00','2017-05-30 03:29:47');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `youtube_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `votes` int(11) NOT NULL DEFAULT '0',
  `seen` int(11) NOT NULL DEFAULT '0',
  `commented` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `weight` tinyint(4) NOT NULL DEFAULT '0',
  `next_id` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `videos_slug_unique` (`slug`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` VALUES (2,'0srMl2b9bUk','https://www.youtube.com/watch?v=0srMl2b9bUk',0,0,0,0,'2016-11-01 04:19:13','2016-11-01 04:19:13','the-cat-stubborn-1477973953','The cat stubborn',0,0,3),(3,'tj2Yr09nOcM','https://www.youtube.com/watch?v=tj2Yr09nOcM',0,0,0,0,'2016-11-01 04:21:07','2016-11-01 04:21:07','my-pet-1477974067','My Pet',0,0,4),(4,'JU1zDHPPnUU','https://www.youtube.com/watch?v=JU1zDHPPnUU',0,0,0,0,'2016-11-01 19:33:01','2016-11-01 19:33:01','funny-pet-moment-so-kute-1478028781','[Funny Pet Moment] So kute ',0,0,3);
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-20  4:17:22

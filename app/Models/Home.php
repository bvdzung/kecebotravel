<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Home extends Model
{
    protected $table = 'home';

    protected $fillable = ['body_en', 'body_fr'];

    public function getAttributeWithLocale($key)
    {
        return $this->getAttribute($key . '_' . App::getLocale());
    }
}

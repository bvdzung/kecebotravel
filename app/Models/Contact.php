<?php

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public $fillable = ['body_en', 'body_fr'];

    public function getAttributeWithLocale($key)
    {
        return $this->getAttribute($key . '_' . App::getLocale());
    }
}

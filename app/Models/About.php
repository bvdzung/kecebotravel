<?php

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table = 'about';

    protected $fillable = ['body_en', 'body_fr'];

    public function getAttributeWithLocale($key)
    {
        return $this->getAttribute($key . '_' . App::getLocale());
    }
}

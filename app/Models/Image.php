<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

/**
 * @property mixed path
 * @property mixed thumbnail
 */
class Image extends Model
{
    protected $fillable = [
        "id",
        "type",
        "path",
        "thumbnail"
    ];

    public function remove()
    {
        $this->delete();

        try {
            unlink(public_path($this->path));
            unlink(public_path($this->thumbnail));
        } catch (\Exception $e) {

        }
    }
}

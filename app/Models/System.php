<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class System extends Model
{
    protected $table = 'systems';

    protected $fillable = [
        'title_en',
        'title_fr',
        'description_en',
        'description_fr',
        'footer_en',
        'footer_fr',
        'favicon',
        'email',
        'phone',
        'mobile',
    ];

    public function getAttributeWithLocale($key)
    {
        return $this->getAttribute($key . '_' . App::getLocale());
    }
}

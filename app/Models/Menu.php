<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Menu extends Model
{
    protected $fillable = ['weight', 'active'];

    public function getAttributeWithLocale($key)
    {
        return $this->getAttribute($key . '_' . App::getLocale());
    }
}

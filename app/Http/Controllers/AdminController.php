<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\Contact;
use App\Models\Home;
use App\Models\Image;
use App\Models\Menu;
use App\Models\System;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as ImageResize;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function welcome()
    {
        return view('admin.welcome');
    }


    public function contact(Request $request)
    {
        $contact = Contact::first();

        return view('admin.contact', compact('contact'));
    }


    public function updateContact(Request $request)
    {
        if ($request->isMethod('post')) {
            $contact = Contact::first();
            $contact->fill($request->all())->update();
        }

        return back()->with('success', 'Update success!');
    }

    public function updateLodge(Request $request)
    {
        if ($request->isMethod('post')) {
            $lodge = About::first();
            $lodge->fill($request->all())->update();
        }

        return back()->with('success', 'Update success!');
    }

    public function lodge(Request $request)
    {
        $lodge = About::first();

        return view('admin.lodge', compact('lodge'));
    }

    public function updateHome(Request $request)
    {
        if ($request->isMethod('post')) {
            $home = Home::first();
            $home->fill($request->all())->update();
        }

        return back()->with('success', 'Update success!');
    }

    public function home(Request $request)
    {
        $home = Home::first();

        return view('admin.home', compact('home'));
    }

    public function system(Request $request)
    {
        $system = System::first();

        return view('admin.system', compact('system'));
    }

    public function updateSystem(Request $request)
    {
        if ($request->isMethod('post')) {
            $system = System::first();
            $system->fill($request->all())->update();
        }

        return back()->with('success', 'Update success!');
    }

    public function menu()
    {
        $menus = Menu::orderBy('weight')->get();

        return view('admin.menu', compact('menus'));
    }

    public function updateMenu(Request $request)
    {
        $menus = Menu::orderBy('weight')->get();

        $data = $request->get('data');

        foreach ($menus as $menu) {
            $menu->fill($data[$menu->id])->save();
        }

        return back()->with('success', 'Update success!');
    }

    public function user(Request $request)
    {
        $users = User::all();

        return view('admin.user', compact('users'));
    }

    public function userRegister()
    {
        return view('auth.register');
    }

    public function userStore(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
        ]);

        return redirect(route('admin.user'))->with('success', 'Update success!');
    }

    public function image()
    {
        $images = Image::all();

        return view('admin.images.index', compact('images'));
    }

    public function imageAdd()
    {
        return view('admin.images.add');
    }

    public function imageStore(Request $request)
    {
        $image = ImageResize::make($request->file);
        
        $string = str_random(12) . Carbon::now()->timestamp;

        $path = public_path('media/') . $string . '.jpg';

        $thumbnail = public_path('media/thumbnail/') . $string . '.jpg';

        if ($request->type == 2) {
            $image->save($path);
        } else {
            $image->resize(config('image.size')[$request->type]['width'], config('image.size')[$request->type]['height'])->save($path);
        }

        $image->resize(100, 100)->save($thumbnail);

        Image::create(array_merge($request->only('type'), [
            'path' => '/media/' . $string . '.jpg',
            'thumbnail' => '/media/thumbnail/' . $string . '.jpg'
        ]));

        return redirect(route('admin.image.index'))->with('success', 'Update success!');
    }

    public function imageRemove(Request $request, $id)
    {
        Image::findOrFail($id)->remove();

        return redirect(route('admin.image.index'))->with('success', 'Remove success!');

    }


}

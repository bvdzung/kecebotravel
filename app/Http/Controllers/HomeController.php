<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\Contact;
use App\Models\Detail;
use App\Models\Home;
use App\Models\Image;
use Illuminate\Support\Facades\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //Add aut
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home = Home::first();

        $images = Image::where('type', 0)->get();

        return view('home', compact('home', 'images'));
    }

    public function detail()
    {
        $type = session('type', 1);

        $details = Detail::all();

        $detail = $details->where('type', $type)->first();

        $images = Image::whereIn('type', [2, 3, 4, 5])->get();

        return view('detail', compact('details', 'detail', 'images', 'type'));
    }

    public function about()
    {
        $about = About::first();

        return view('about', compact('about'));
    }

    public function contact()
    {
        $contact = Contact::first();

        return view('contact', compact('contact'));
    }

    public function gallery()
    {
        return view('gallery');
    }

    public function lodge()
    {
        $about = About::first();

        $image = Image::where('type', 6)->first();

        return view('about', compact('about', 'image'));
    }

    public function ajax()
    {
        $images = Image::where('type', 1)->get();
        $result = [];

        foreach ($images as $image) {

            $result[] = [
                'url' => url($image->path),
                'thumbnail' => url($image->thumbnail),
                'title' => $image->getAttribute('title', 'No title')
            ];

        }
        return response()->json($result);
    }

    public function changeType($type)
    {
        session([
            'type' => $type
        ]);

        return back();
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: Piter
 * Date: 1/10/2017
 * Time: 6:15 AM
 */

namespace App\Providers;


use App\Models\Menu;
use App\Models\System;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewProvider extends ServiceProvider
{
    public function boot()
    {

        View::share('menus', Menu::where('active', 1)->orderBy('weight')->get());
        View::share('system', System::first());
    }


    public function register()
    {

    }
}
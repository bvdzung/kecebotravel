<?php


return [

    'type' => [
        0 => 'Home Slide',
        1 => 'Gallery',
        2 => 'Stilt House Slide',
        3 => 'Stilt House Avatar',
        4 => 'Bungalows Slide',
        5 => 'Bungalows Avatar',
        6 => 'Lodge ',
    ],

    'size' => [
        0 => [
            'width' => 1140,
            'height' => 500,
        ],
        1 => [
            'width' => '100%',
            'height' => '100%',
        ],
        2 => [
            'width' => 700,
            'height' => 460,
        ],
        3 => [
            'width' => 276,
            'height' => 140,
        ],
        4 => [
            'width' => 700,
            'height' => 460,
        ],
        5 => [
            'width' => 276,
            'height' => 140,
        ],
        6 => [
            'width' => 821,
            'height' => 314,
        ],
    ]
];
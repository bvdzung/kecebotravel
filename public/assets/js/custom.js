jQuery(document).ready(function ($) {

    "use strict";

    $('.owl-carousel').owlCarousel({
        slideSpeed: 600,
        pagination: true,
        paginationSpeed: 400,
        addClassActive: true,
        singleItem: true,
        navigation: false,
        autoPlay:true,
    });
});
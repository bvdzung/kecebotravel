<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return redirect(route('home'));
});

Route::get('home', ['uses' => 'HomeController@index', 'as' => 'home']);
Route::get('detail', ['uses' => 'HomeController@detail', 'as' => 'detail']);
Route::get('about', ['uses' => 'HomeController@about', 'as' => 'about']);
Route::get('contact', ['uses' => 'HomeController@contact', 'as' => 'contact']);
Route::get('gallery', ['uses' => 'HomeController@gallery', 'as' => 'gallery']);
Route::get('changeType/{type}', ['uses' => 'HomeController@changeType', 'as' => 'changeType']);
Route::get('lodge', ['uses' => 'HomeController@lodge', 'as' => 'lodge']);
Route::get('ajax', ['uses' => 'HomeController@ajax', 'as' => 'ajax']);

Route::get('changeLanguage/{language}', ['uses' => 'LanguageController@changeLanguage', 'as' => 'changeLanguage']);
//Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Route::post('login', ['uses' => 'Auth\LoginController@login', 'as' => 'login']);
    Route::get('login', ['uses' => 'Auth\LoginController@showLoginForm', 'as' => 'login']);
    Route::post('logout', ['uses' => 'Auth\LoginController@logout', 'as' => 'logout']);
    Route::get('contact', ['uses' => 'AdminController@contact', 'as' => 'admin.contact']);
    Route::get('lodge', ['uses' => 'AdminController@lodge', 'as' => 'admin.lodge']);
    Route::get('home', ['uses' => 'AdminController@home', 'as' => 'admin.home']);
    Route::get('logout', ['uses' => 'Auth\LoginController@logout', 'as' => 'admin.logout']);
    Route::get('system', ['uses' => 'AdminController@system', 'as' => 'admin.system']);
    Route::get('/', ['uses' => 'AdminController@welcome', 'as' => 'admin.welcome']);
    Route::get('menu', ['uses' => 'AdminController@menu', 'as' => 'admin.menu']);
    Route::get('image', ['uses' => 'AdminController@image', 'as' => 'admin.image.index']);
    Route::get('image/{id}/remove', ['uses' => 'AdminController@imageRemove', 'as' => 'admin.image.remove']);
    Route::get('user', ['uses' => 'AdminController@user', 'as' => 'admin.user']);
    Route::get('userRegister', ['uses' => 'AdminController@userRegister', 'as' => 'admin.userRegister']);
    Route::post('updateContact', ['uses' => 'AdminController@updateContact', 'as' => 'admin.updateContact']);
    Route::post('updateLodge', ['uses' => 'AdminController@updateLodge', 'as' => 'admin.updateLodge']);
    Route::post('updateHome', ['uses' => 'AdminController@updateHome', 'as' => 'admin.updateHome']);
    Route::post('updateSystem', ['uses' => 'AdminController@updateSystem', 'as' => 'admin.updateSystem']);
    Route::post('updateMenu', ['uses' => 'AdminController@updateMenu', 'as' => 'admin.updateMenu']);
    Route::post('userStore', ['uses' => 'AdminController@userStore', 'as' => 'admin.userStore']);
    Route::post('userStore', ['uses' => 'AdminController@userStore', 'as' => 'admin.userStore']);
    Route::get('imageAdd', ['uses' => 'AdminController@imageAdd', 'as' => 'admin.image.add']);
    Route::post('imageStore', ['uses' => 'AdminController@imageStore', 'as' => 'admin.image.store']);


});


